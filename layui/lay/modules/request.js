/** layui-v2.2.5 MIT License By https://www.layui.com */ ;
layui.define("request", function(e) {
	"use strict";
	var $ = layui.$;
	var	request =  function(url, data = {}, header = "application/x-www-form-urlencoded"){
		return new Promise(function(resolve, reject) {
	    wx.request({
	      url: url,
	      data: data,
	      header: {
	        'Content-Type': header,
	        'X-token': window.sessionStorage.getItem("token")
	      },
	      success: function(res) {
	        res = res.data;
	        if (res.code == 10010) {
	        	alert("登录身份失效，请重新登录");
	        	window.location.href="/environment-admin/page/login/login.html"
	          })
	        } else
	          resolve(res);
	      },
	      fail: function(err) {
	        wx.hideLoading();
	        reject(err)
	      }
	    })
	  });
	}
	e("request", request)
});