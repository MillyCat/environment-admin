
layui.use(['form','layer','upload','laydate',"address",'util'],function(){
    form = layui.form;
    $ = layui.jquery;
    var layer = parent.layer === undefined ? layui.layer : top.layer,
        upload = layui.upload,
        laydate = layui.laydate,
        address = layui.address;
	var util = layui.util,
		api = util.api;

	
    var id = util.getQueryString("id");
    
	util.request(api.getFarmPageInfo).then(function(res) {
		var s="";
		$.each(res.result.authorityList, function(index,value) {
			s+=`<input type="radio" name="authority" value="${value.id}" title="${value.name}" lay-skin="primary" />`
		});
		$('.authorityInput').html(s);
	    util.request(api.getUserInfoById,{id:id}).then(function(res) {
			$('.name').val(res.result.user.name);
			$('.empno').val(res.result.user.empno);
			$('.sex').val(res.result.user.sexName);
			$('.phone').val(res.result.user.phone);
			$('.email').val(res.result.user.email);
			$('.image').attr('src',api.imageHost+res.result.user.headUrl);
			$(`.authorityInput input[value=${res.result.user.authority}]`).attr('checked',true);
			form.render();
	    })
	})
	
	$('.submitBtn').click(function(){
		if(typeof($(`.authorityInput input[name=authority]:checked`).val())=="undefined"){
			layer.alert("选择权限"); return;
		}
		util.request(api.updateAuthority,{
			id:id,
			authority:$(`.authorityInput input[name=authority]:checked`).val()
		}).then(function(res) {
			layer.alert("修改用户权限成功",function(index){
				window.parent.location.reload();
				layer.close(index);
			})
		})
	})
    
		
})