layui.use(['form', 'layer', 'laydate', 'table', 'laytpl', 'util'], function() {
	var form = layui.form,
		layer = parent.layer === undefined ? layui.layer : top.layer,
		$ = layui.jquery,
		laydate = layui.laydate,
		laytpl = layui.laytpl,
		table = layui.table;
	var util = layui.util,
		api = util.api;

	var tableIns = table.render({
		elem: '#userList',
		cellMinWidth: 95,
		page: true,
		height: "full-125",
		limit: 20,
		limits: [10, 15, 20, 25],
		id: "userList",
		cols: [
			[
				{
					field: 'id',
					title: 'ID',
					width: 60,
					align: "center"
				},
				{
					field: 'name',
					title: '姓名',
					align: "center"
				},
				{
					field: 'empno',
					title: '工号',
					align: "center"
				},
				{
					field: 'sexName',
					title: '性别',
					align: "center"
				},
				{
					field: 'phone',
					title: '手机号',
					align: "center"
				},
				{
					field: 'email',
					title: '邮箱',
					align: "center"
				},
				{
					field: 'applyTime',
					title: '申请时间',
					align: 'center'
				},
				{
					title: '操作',
					width: 200,
					templet: '#userListBar',
					fixed: "right",
					align: "center"
				}

			]
		]
	});

	util.request(api.getUserList).then(function(res) {
		var datas = res.result.userList
		table.reload("userList", {
			data: datas
		})
	})
	
	$(".search_btn").on("click", function() {
		util.request(api.getUserList, {
			name: $("#t1").val(),
			empno: $("#t2").val(),
			phone: $("#t3").val()
		}).then(function(res) {
		var datas = res.result.userList
			table.reload("userList", {
				data: datas
			})
		})

	});


	//列表操作
	table.on('tool(userList)', function(obj) {
		var layEvent = obj.event,
			data = obj.data;
		if(layEvent === 'admin') { 
			layer.confirm('确定设为管理员？', {
				icon: 3,
				title: '提示信息'
			}, function(index) {
				
				util.request(api.agreeAdmin,{
					id:data.id
				}).then(function(res) {
					layer.alert("操作成功",function(index){
						window.location.reload();
	  					layer.close(index);
					})
				})
			});
		} else if(layEvent === 'freeze') { 
			layer.confirm('确定冻结改用户？', {
				icon: 3,
				title: '提示信息'
			}, function(index) {
				
				util.request(api.freeze,{
					id:data.id
				}).then(function(res) {
					layer.alert("操作成功",function(index){
						window.location.reload();
	  					layer.close(index);
					})
				})
			});
		} else if(layEvent === 'unfreeze') { 
			console.log(api.unFreeze)
			layer.confirm('确定解冻该用户？', {
				icon: 3,
				title: '提示信息'
			}, function(index) {
				
				util.request(api.unFreeze,{
					id:data.id
				}).then(function(res) {
					layer.alert("操作成功",function(index){
						window.location.reload();
	  					layer.close(index);
					})
				})
			});
		}else if(layEvent === 'look'){
			var index = layui.layer.open({
				title: "用户详情",
				type: 2,
				content: "userDetail.html?id="+data.id,
				success: function(layero, index) {
					var body = layui.layer.getChildFrame('body', index);
				},
			})
			layui.layer.full(index);
			$(window).on("resize", function() {
				layui.layer.full(index);
			})
		}
	});


})