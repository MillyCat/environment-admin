layui.use(['form', 'layer', 'jquery', "util"], function() {
	var form = layui.form,
		layer = parent.layer === undefined ? layui.layer : top.layer,
		$ = layui.jquery;
	var	util = layui.util,
	 	api = util.api;

	//登录按钮
	form.on("submit(login)", function(data) {
		var that = $(this);
		$(this).text("登录中...").attr("disabled", "disabled").addClass("layui-disabled");

		$.post({
			url: api.login,
			data: {
				loginId: $("#userName").val(),
				password: $("#password").val()
			},
			header: {
				'Content-Type': "application/x-www-form-urlencoded",
			},
			success: function(res) {
				console.log(res)
				if(res.code == 200) {
					window.sessionStorage.setItem("token", res.result.token)
					window.location.href = "/environment-admin/index.html";
				} else {
					that.text("登录").removeAttr("disabled").removeClass("layui-disabled");
					layer.alert(res.message);
				}
			},
			fail: function(err) {
				that.text("登录").removeAttr("disabled").removeClass("layui-disabled");
				reject(err)
			}
		})
		return false;
	})

	//表单输入效果
	$(".loginBody .input-item").click(function(e) {
		e.stopPropagation();
		$(this).addClass("layui-input-focus").find(".layui-input").focus();
	})
	$(".loginBody .layui-form-item .layui-input").focus(function() {
		$(this).parent().addClass("layui-input-focus");
	})
	$(".loginBody .layui-form-item .layui-input").blur(function() {
		$(this).parent().removeClass("layui-input-focus");
		if($(this).val() != '') {
			$(this).parent().addClass("layui-input-active");
		} else {
			$(this).parent().removeClass("layui-input-active");
		}
	})
})