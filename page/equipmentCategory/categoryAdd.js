layui.use(['form','layer','layedit','laydate','upload','util'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        laypage = layui.laypage,
        layedit = layui.layedit,
        laydate = layui.laydate,
        $ = layui.jquery;
	var util = layui.util,
		api = util.api;

    
    var id = util.getQueryString("id");
    
    if(id){
		util.request(api.getEquipmentCategoryById,{
			id:id
		}).then(function(res) {
			$(".name").val(res.result.equipmentCategory.name);
			$(".describe").val(res.result.equipmentCategory.describe);
			$(".element").val(res.result.equipmentCategory.element);
			$(".units").val(res.result.equipmentCategory.describe);
			$(".maxLimit").val(res.result.equipmentCategory.maxLimit);
			$(".minLimit").val(res.result.equipmentCategory.minLimit);
		})
    }
	
	$('.submitBtn').click(function(){
		if(id){
			util.request(api.updateEquipmentCategory,{
				id:id,
				name:$(".name").val(),
				describe:$(".describe").val(),
				element:$(".element").val(),
				units:$(".describe").val(),
				maxLimit:$(".maxLimit").val(),
				minLimit:$(".minLimit").val()
			}).then(function(res) {
				layer.alert("操作成功",function(index){
					window.parent.location.reload();
  					layer.close(index);
				})
			})
		}else{
			util.request(api.addEquipmentCategory,{
				name:$(".name").val(),
				describe:$(".describe").val(),
				element:$(".element").val(),
				units:$(".describe").val(),
				maxLimit:$(".maxLimit").val(),
				minLimit:$(".minLimit").val()
			}).then(function(res) {
				layer.alert("操作成功",function(index){
					window.parent.location.reload();
  					layer.close(index);
				})
			})
		}
	})
})