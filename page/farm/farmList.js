layui.use(['form', 'layer', 'laydate', 'table', 'laytpl','util'], function() {
	var form = layui.form,
		layer = parent.layer === undefined ? layui.layer : top.layer,
		$ = layui.jquery,
		laydate = layui.laydate,
		laytpl = layui.laytpl,
		table = layui.table;
	var util = layui.util,
		api = util.api;

	var tableIns = table.render({
		elem: '#farmList',
		cellMinWidth: 95,
		page: true,
		height: "full-125",
		limit: 20,
		limits: [10, 15, 20, 25],
		id: "farmList",
		cols: [
			[{
					type: "checkbox",
					width: 50,
					fixed: "left"
				},
				{
					field: 'id',
					width: 60,
					title: 'ID',
					align: "center"
				},
				{
					field: 'name',
					title: '农场名称',
					align: "center"
				},
				{
					field: 'describe',
					title: '农场描述',
					align: 'center'
				},
				{
					field: 'farmCategoryName',
					title: '农场分类',
					align: 'center'
				},
				{
					field: 'authorityName',
					title: '权限',
					align: 'center'
				},
				{
					field: 'creatorName',
					title: '创建人',
					align: 'center'
				},
				{
					field: 'createTime',
					title: '创建时间',
					align: 'center',
					templet: function(d) {
						return d.createTime.substring(0, 10);
					}
				},
				{
					title: '操作',
					width: 170,
					templet: '#farmListBar',
					fixed: "right",
					align: "center"
				}

			]
		]
	});

	util.request(api.getFarmList).then(function(res) {
		if(res.code == 200) {
			var datas = res.result.farmList
			table.reload("farmList", {
				data: datas
			})
		} else {
			layer.alert(res.message);
		}
	})

	$(".search_btn").on("click", function() {
		util.request(api.getFarmList, {
			name: $("#t1").val(),
			describe: $("#t2").val(),
			creatorName: $("#t3").val(),
			authorityName: $("#t4").val(),
			farmCategoryName: $("#t5").val(),
		}).then(function(res) {
			var datas = res.result.farmList;
			table.reload("farmList", {
				page: {
					curr: 1 //重新从第 1 页开始
				},
				data: datas
			})

		})

	});
	
	
	$(".delAll_btn").click(function() {
		var checkStatus = table.checkStatus('farmList'),
			data = checkStatus.data,
			ids = [];

		if(data.length > 0) {
			for(var i in data) {
				ids.push(data[i].id);
			}
			layer.confirm('确定删除选中农场？', {
				icon: 3,
				title: '提示信息'
			}, function(index) {
				console.log(JSON.stringify(ids))
				util.request(api.deleteFarmList,{
					ids:JSON.stringify(ids)
				}).then(function(res) {
					layer.alert("操作成功",function(index){
						window.location.reload();
	  					layer.close(index);
					})
				})
			})
		} else {
			layer.msg("请选择需要删除农场");
		}
	})
	
	table.on('tool(farmList)', function(obj) {
		var layEvent = obj.event,
			data = obj.data;
		if(layEvent === 'edit') { //编辑
			addFarm(data);
		} else if(layEvent === 'del') { //删除
			layer.confirm('确定删除此农场？', {
				icon: 3,
				title: '提示信息'
			}, function(index) {
				util.request(api.deleteFarmById,{
					id:data.id
				}).then(function(res) {
					layer.alert("操作成功",function(index){
						window.location.reload();
	  					layer.close(index);
					})
				})
			});
		} else if(layEvent === 'look') { //预览
			layer.alert("此功能需要前台展示，实际开发中传入对应的必要参数进行文章内容页面访问")
		}
	});

	
	$(".add_btn").click(function() {
		addFarm();
	})
	
	function addFarm(edit) {
		var url="farmAdd.html";
		if(edit) {
			url+="?id="+edit.id;
		}
		var index = layui.layer.open({
			title: "添加农场",
			type: 2,
			content: url,
			success: function(layero, index) {
				var body = layui.layer.getChildFrame('body', index);
			},
		})
		layui.layer.full(index);
		$(window).on("resize", function() {
			layui.layer.full(index);
		})
	}


})