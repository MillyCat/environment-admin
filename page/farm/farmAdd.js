layui.use(['form','layer','layedit','laydate','upload','util','element'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        laypage = layui.laypage,
        upload = layui.upload,
        layedit = layui.layedit,
        laydate = layui.laydate,
        $ = layui.jquery,
    	element = layui.element;
	var util = layui.util,
		api = util.api;

    var id = util.getQueryString("id");


	var equipmentObject=[];
	util.request(api.getFarmPageInfo).then(function(res) {
		var s="";
		$.each(res.result.farmCategoryList, function(index,value) {
			s+=`<input type="radio" name="farmCategory" value="${value.id}" title="${value.name}" lay-skin="primary" />`
		});
		$('.farmCategoryInput').html(s);
		s=""
		$.each(res.result.authorityList, function(index,value) {
			s+=`<input type="radio" name="authority" value="${value.id}" title="${value.name}" lay-skin="primary" />`
		});
		$('.authorityInput').html(s);
		s=`<option value="">直接选择或搜索选择</option>`
		$.each(res.result.equipmentList, function(index,value) {
			s+=`<option value="${value.id}">${value.name}</option>`
		});
		$('.equipmentInput').html(s)
		
	    if(id){
			util.request(api.getFarmById,{
				id:id
			}).then(function(res) {
				$('.name').val(res.result.farm.name);
				$('.describe').val(res.result.farm.name);
				$('.image').attr('src',api.imageHost+res.result.farm.imageUrl);
				$('.image').attr('imgName',res.result.farm.imageUrl);
				$('.QRcode').attr('src',api.imageHost+res.result.farm.codeUrl);
				
				$(`.farmCategoryInput input[value=${res.result.farm.farmCategory}]`).attr('checked',true);
				$(`.authorityInput input[value=${res.result.farm.authority}]`).attr('checked',true);
				
				$.each(res.result.farm.equipmentList, function(index,value) {
					equipmentObject.push({id:value.id,name:value.name})
				});
				equipmentRender()
				form.render();
				
			})
	   }else{
			form.render();
	    }
	});
	
	$(".addEquipment_btn").click(function(){
		var id=$('.equipmentInput').val();
		var name=$(`.equipmentInput option[value=${id}]`).text();
		var flag =false;
		equipmentObject.forEach(item=>{
		    if(item.id==id){
		        flag =true
		    }
		})
		if(!flag) equipmentObject.push({id:id,name:name})
		equipmentRender()
		
	})
	$(".deleteEquipment_btn").click(function(){
		var index=$('.equipmentRender input[name="e"]:checked').attr('index');
		equipmentObject.splice(index,1);
		equipmentRender()
		
	})
	function equipmentRender(){
		var s='';
		$.each(equipmentObject, function(index,value) {
			s +=`<input type="radio" name="e" value="${value.id}" title="${value.name}" index="${index}" lay-skin="primary">`
		});
		$('.equipmentRender').empty().html(s);
		form.render();
	}

    upload.render({
        elem: '#image',
        url:api.uploadImage,
        accept: 'images',
        done: function(res){
			$('.image').attr('src',api.imageHost+res.result.fileName);
			$('.image').attr('imgName',res.result.fileName);
        }
    });
    
    
	$('.submitBtn').click(function(){
		if($('.name').val().trim()==""){
			layer.alert("名称不能为空"); return;
		}
		if($('.describe').val().trim()==""){
			layer.alert("描述不能为空"); return;
		}
		if(typeof($('.image').attr('imgName'))=="undefined"){
			layer.alert("图片不能为空"); return;
		}
		if(typeof($(`.farmCategoryInput input[name=farmCategory]:checked`).val())=="undefined"){
			layer.alert("选择农场类型"); return;
		}
		if(typeof($(`.authorityInput input[name=authority]:checked`).val())=="undefined"){
			layer.alert("选择权限"); return;
		}
		if(id){
			var data={
				id:id,
				name:$('.name').val(),
				describe:$('.describe').val(),
				imageUrl:$('.image').attr('imgName'),
				farmCategory:$(`.farmCategoryInput input[name=farmCategory]:checked`).val(),
				authority:$(`.authorityInput input[name=authority]:checked`).val(),
				equipmentList:[],
			}
			for(var index in equipmentObject){
				data.equipmentList.push({id:equipmentObject[index].id})
			}
			console.log(data)
			util.request(api.updateFarm,JSON.stringify(data),"application/json").then(function(res) {
				layer.alert("操作成功",function(index){
					window.parent.location.reload();
					layer.close(index);
				})
			})
		}else{
			var data={
				name:$('.name').val(),
				describe:$('.describe').val(),
				imageUrl:$('.image').attr('imgName'),
				farmCategory:$(`.farmCategoryInput input[name=farmCategory]:checked`).val(),
				authority:$(`.authorityInput input[name=authority]:checked`).val(),
				equipmentList:[],
			}
			for(var index in equipmentObject){
				data.equipmentList.push({id:equipmentObject[index].id})
			}
			console.log(data)
			util.request(api.addFarm,JSON.stringify(data),"application/json").then(function(res) {
				layer.alert("操作成功",function(index){
					window.parent.location.reload();
					layer.close(index);
				})
			})
		}
	})
    
})