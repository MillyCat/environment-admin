layui.use(['form', 'layer', 'laydate', 'table', 'laytpl','util'], function() {
	var form = layui.form,
		layer = parent.layer === undefined ? layui.layer : top.layer,
		$ = layui.jquery,
		laydate = layui.laydate,
		laytpl = layui.laytpl,
		table = layui.table;
	var util = layui.util,
		api = util.api;



	var tableIns = table.render({
		elem: '#authorityList',
		cellMinWidth: 95,
		page: true,
		height: "full-125",
		limit: 20,
		limits: [10, 15, 20, 25],
		id: "authorityList",
		cols: [
			[
				{
					field: 'id',
					title: 'ID',
					width: 60,
					align: "center"
				},
				{
					field: 'name',
					title: '权限名称',
					align: "center"
				},
				{
					field: 'level',
					title: '权限等级',
					align: 'center'
				},
			]
		]
	});


	util.request(api.getAuthorityList).then(function(res) {
		var datas = res.result.authorityList
		table.reload("authorityList", {
			data: datas
		})
	})
	
})