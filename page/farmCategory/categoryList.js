layui.use(['form', 'layer', 'table', 'laytpl', 'util', 'jquery'], function() {
	var form = layui.form,
		layer = parent.layer === undefined ? layui.layer : top.layer,
		$ = layui.jquery,
		laydate = layui.laydate,
		laytpl = layui.laytpl,
		table = layui.table;
	var util = layui.util,
		api = util.api;

	var tableIns = table.render({
		elem: '#categoryList',
		cellMinWidth: 95,
		page: true,
		height: "full-0",
		limit: 20,
		limits: [10, 15, 20, 25],
		id: "categoryList",
		cols: [
			[{
					type: "checkbox",
					fixed: "left",
					width: 50
				},
				{
					field: 'id',
					title: 'ID',
					width: 60,
					align: "center"
				},
				{
					field: 'name',
					title: '分类名称',
					align: "center"
				},
				{
					field: 'describe',
					title: '分类描述',
					align: 'center'
				},
				{
					field: 'creatorName',
					title: '创建人',
					align: 'center'
				},
				{
					field: 'createTime',
					title: '创建时间',
					align: 'center',
					templet: function(d) {
						return d.createTime.substring(0, 10);
					}
				},
				{
					title: '操作',
					width: 170,
					templet: '#categoryListBar',
					fixed: "right",
					align: "center"
				}
			]
		]
	});;

	util.request(api.getFarmCategoryList).then(function(res) {
		if(res.code == 200) {
			var datas = res.result.farmCategoryList
			table.reload("categoryList", {
				data: datas
			})
		} else {
			layer.alert(res.message);
		}
	})

	$(".search_btn").on("click", function() {
		util.request(api.getFarmCategoryList, {
			name: $("#t1").val(),
			describe: $("#t2").val(),
			creatorName: $("#t3").val()
		}).then(function(res) {
			var datas = res.result.farmCategoryList;
			table.reload("categoryList", {
				page: {
					curr: 1 //重新从第 1 页开始
				},
				data: datas
			})

		})

	});

	$(".delAll_btn").click(function() {
		var checkStatus = table.checkStatus('categoryList'),
			data = checkStatus.data,
			ids = [];

		if(data.length > 0) {
			for(var i in data) {
				ids.push(data[i].id);
			}
			layer.confirm('确定删除选中分类？', {
				icon: 3,
				title: '提示信息'
			}, function(index) {
				console.log(JSON.stringify(ids))
				util.request(api.deleteFarmCategoryList,{
					ids:JSON.stringify(ids)
				}).then(function(res) {
					layer.alert("操作成功",function(index){
						window.location.reload();
	  					layer.close(index);
					})
				})
			})
		} else {
			layer.msg("请选择需要删除分类");
		}
	})

	//列表操作
	table.on('tool(categoryList)', function(obj) {
		var layEvent = obj.event,
			data = obj.data;
		if(layEvent === 'edit') { //编辑
			addCategory(data);
		} else if(layEvent === 'del') { //删除
			layer.confirm('确定删除此分类？', {
				icon: 3,
				title: '提示信息'
			}, function(index) {
				
				util.request(api.deleteFarmCategoryById,{
					id:data.id
				}).then(function(res) {
					layer.alert("操作成功",function(index){
						window.location.reload();
	  					layer.close(index);
					})
				})
			});
		}
	});

    $(".add_btn").click(function(){
        addCategory();
    })
    
	function addCategory(edit) {
		var url="categoryAdd.html";
		if(edit) {
			console.log(edit)
			url+="?id="+edit.id;
		}
		var index = layui.layer.open({
			title: "添加农场分类",
			type: 2,
			content: url,
			area: ['400px', '300px'],
			success: function(layero, index) {
				var body = layui.layer.getChildFrame('body', index);
			},
		})
	}
})