layui.use(['form','layer','layedit','laydate','upload','util'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        laypage = layui.laypage,
        layedit = layui.layedit,
        laydate = layui.laydate,
        $ = layui.jquery,
        util=layui.util,
        api=util.api;
        
    var id = util.getQueryString("id");
    if(id){
		util.request(api.getFarmCategoryById,{
			id:id
		}).then(function(res) {
			$(".name").val(res.result.farmCategory.name);
			$(".describe").val(res.result.farmCategory.describe);
		})
    }
	
	$('.submitBtn').click(function(){
		if(id){
			util.request(api.updateFarmCategory,{
				id:id,
				name:$(".name").val(),
				describe:$(".describe").val()
			}).then(function(res) {
				layer.alert("操作成功",function(index){
					window.parent.location.reload();
  					layer.close(index);
				})
			})
		}else{
			util.request(api.addFarmCategory,{
				name:$(".name").val(),
				describe:$(".describe").val()
			}).then(function(res) {
				layer.alert("操作成功",function(index){
					window.parent.location.reload();
  					layer.close(index);
				})
			})
		}
	})

    
})