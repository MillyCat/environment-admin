layui.use(['form', 'layer', 'laydate', 'table', 'laytpl', 'util'], function() {
	var form = layui.form,
		layer = parent.layer === undefined ? layui.layer : top.layer,
		$ = layui.jquery,
		laydate = layui.laydate,
		laytpl = layui.laytpl,
		table = layui.table;
	var util = layui.util,
		api = util.api;

	//新闻列表
	var tableIns = table.render({
		elem: '#planList',
		cellMinWidth: 95,
		page: true,
		height: "full-125",
		limit: 20,
		limits: [10, 15, 20, 25],
		id: "planList",
		cols: [
			[
				{
					field: 'id',
					title: 'ID',
					width: 60,
					align: "center"
				},
				{
					field: 'title',
					title: '方案名称',
					align: "center"
				},
				{
					field: 'describe',
					title: '方案描述',
					align: 'center'
				},
				{
					field: 'creatorName',
					title: '创建人',
					align: 'center'
				},
				{
					field: 'createTime',
					title: '创建时间',
					align: 'center',
					templet: function(d) {
						return d.createTime.substring(0, 10);
					}
				},
				{
					title: '操作',
					width: 170,
					templet: '#planListBar',
					fixed: "right",
					align: "center"
				}

			]
		]
	});


	util.request(api.getPlanList).then(function(res) {
		if(res.code == 200) {
			var datas = res.result.planList
			table.reload("planList", {
				data: datas
			})
		} else {
			layer.alert(res.message);
		}
	})

	$(".search_btn").on("click", function() {
		util.request(api.getPlanList, {
			title: $("#t1").val(),
			describe: $("#t2").val(),
			creatorName: $("#t3").val(),
		}).then(function(res) {
			var datas = res.result.planList;
			table.reload("planList", {
				page: {
					curr: 1 //重新从第 1 页开始
				},
				data: datas
			})

		})

	});
	
	
	
	function addPlan(edit) {
		var index = layui.layer.open({
			title: "方案详情",
			type: 2,
			content: "planAdd.html?id="+edit.id,
			success: function(layero, index) {
				var body = layui.layer.getChildFrame('body', index);
			},
		})
		layui.layer.full(index);
		$(window).on("resize", function() {
			layui.layer.full(index);
		})
	}

	$(".addPlan_btn").click(function() {
		addPlan();
	})

	

	//列表操作
	table.on('tool(planList)', function(obj) {
		var layEvent = obj.event,
			data = obj.data;
		if(layEvent === 'edit') { //编辑
			addPlan(data);
		}
	});

})