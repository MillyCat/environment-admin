layui.use(['form', 'layer','util'], function() {
	var form = layui.form
	layer = parent.layer === undefined ? layui.layer : top.layer,
		laypage = layui.laypage,
		$ = layui.jquery;
	var util = layui.util,
		api = util.api;

	var id = util.getQueryString("id");
	
	util.request(api.getPlanById, {
		id: id
	}).then(function(res) {
		$('.title').val(res.result.plan.title);
		$('.describe').val(res.result.plan.describe);
		var s=''
		$.each(res.result.plan.planDetailList, function(index,value) {
			s+=`<div class="layui-inline">
					<label class="layui-form-label">${value.element}监控范围(${value.unit})</label>
					<div class="layui-input-inline" style="width: 100px;">
						<input disabled type="text" name="temperature_min" autocomplete="off" class="layui-input" value="${value.min}">
					</div>
					<div class="layui-input-inline">-</div>
					<div class="layui-input-inline" style="width: 100px;">
						<input disabled type="text" name="temperature_max" autocomplete="off" class="layui-input" value="${value.max}">
					</div>
				</div>
				<div class="layui-inline">
					<label class="layui-form-label">${value.element}(${value.unit})</label>
					<div class="layui-input-inline" style="width: 100px;">
						<input disabled type="text" name="min" autocomplete="off" class="layui-input" value="${value.value}">
					</div>
				</div>`
		});
		$('.info').empty().html(s);
	})
})