
layui.use(['form','layer','upload','laydate',"address",'util'],function(){
    form = layui.form;
    $ = layui.jquery;
    var layer = parent.layer === undefined ? layui.layer : top.layer,
        upload = layui.upload,
        laydate = layui.laydate,
        address = layui.address;

	var util = layui.util,
		api = util.api;


    upload.render({
        elem: '.userFaceBtn',
        url:api.uploadImage,
        accept: 'images',
        done: function(res){
			$('#userFace').attr('src',api.imageHost+res.result.fileName);
			$('#userFace').attr('imgName',res.result.fileName);
        }
    });
    
	
	util.request(api.getUserInfo).then(function(res) {
		$('.nickname').val(res.result.user.nickname);
		$('.name').val(res.result.user.name);
		$('.empno').val(res.result.user.empno);
		$('.sex').val(res.result.user.sexName);
		$('.phone').val(res.result.user.phone);
		$('.email').val(res.result.user.email);
		$('#userFace').attr('src',api.imageHost+res.result.user.avatarUrl);
		$('#userFace').attr('imgName',res.result.user.avatarUrl);
	})
	
	
	$('.submitBtn').click(function(){
		if($('.nickname').val().trim()==""){
			layer.alert("名称不能为空"); return;
		}
		util.request(api.alterUserInfo,{
			nickname:$('.nickname').val(),
			avatarUrl:$('#userFace').attr('imgName')
		}).then(function(res) {
			layer.alert("操作成功",function(index){
				window.parent.location.reload();
				layer.close(index);
			})
		})
		
	})
	
})