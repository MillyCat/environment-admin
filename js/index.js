var $, tab, dataStr, layer;
layui.config({
	base: "js/"
}).extend({
	"bodyTab": "bodyTab"
})
layui.use(['bodyTab', 'form', 'element', 'layer', 'jquery', "util"], function() {
	var form = layui.form,
		element = layui.element;
	$ = layui.$;
	layer = parent.layer === undefined ? layui.layer : top.layer;
	var util = layui.util,
		api = util.api;
	tab = layui.bodyTab({
		openTabNum: "50", //最大可打开窗口数量
		url: "json/navs.json" //获取菜单json地址
	});

	//判断是否web端打开
	if(!/http(s*):\/\//.test(location.href)) {
		layer.alert("请先将项目部署到 localhost 下再进行访问【建议通过tomcat、webstorm、hb等方式运行，不建议通过iis方式运行】，否则部分数据将无法显示");
	}

	util.request(api.getUserInfo).then(function(res) {
		if(res.code == 200) {
			console.log(res)
			var user=res.result.user;
			$(".userAvatar").attr("src", api.imageHost + user.avatarUrl);
			$(".adminName").text(user.nickname);
		} else {
			layer.alert(res.message);
		}
	})

	function getData(json) {
		$.getJSON(tab.tabConfig.url, function(data) {
			if(json == "farmManagement") {
				dataStr = data.farmManagement;
				//重新渲染左侧菜单
				tab.render();
			} else if(json == "facilityManagement") {
				dataStr = data.facilityManagement;
				//重新渲染左侧菜单
				tab.render();
			} else if(json == "memberManagement") {
				dataStr = data.memberManagement;
				//重新渲染左侧菜单
				tab.render();
			} else if(json == "memberCenter") {
				dataStr = data.memberCenter;
				//重新渲染左侧菜单
				tab.render();
			}
		})
	}
	//页面加载时判断左侧菜单是否显示
	//通过顶部菜单获取左侧菜单
	$(".topLevelMenus li,.mobileTopLevelMenus dd").click(function() {
		if($(this).parents(".mobileTopLevelMenus").length != "0") {
			$(".topLevelMenus li").eq($(this).index()).addClass("layui-this").siblings().removeClass("layui-this");
		} else {
			$(".mobileTopLevelMenus dd").eq($(this).index()).addClass("layui-this").siblings().removeClass("layui-this");
		}
		$(".layui-layout-admin").removeClass("showMenu");
		$("body").addClass("site-mobile");
		getData($(this).data("menu"));
		//渲染顶部窗口
		tab.tabMove();
	})

	//隐藏左侧导航
	$(".hideMenu").click(function() {
		if($(".topLevelMenus li.layui-this a").data("url")) {
			layer.msg("此栏目状态下左侧菜单不可展开"); //主要为了避免左侧显示的内容与顶部菜单不匹配
			return false;
		}
		$(".layui-layout-admin").toggleClass("showMenu");
		//渲染顶部窗口
		tab.tabMove();
	})

	getData("farmManagement");

	//手机设备的简单适配
	$('.site-tree-mobile').on('click', function() {
		$('body').addClass('site-mobile');
	});
	$('.site-mobile-shade').on('click', function() {
		$('body').removeClass('site-mobile');
	});

	// 添加新窗口
	$("body").on("click", ".layui-nav .layui-nav-item a:not('.mobileTopLevelMenus .layui-nav-item a')", function() {
		//如果不存在子级
		if($(this).siblings().length == 0) {
			addTab($(this));
			$('body').removeClass('site-mobile'); //移动端点击菜单关闭菜单层
		}
		$(this).parent("li").siblings().removeClass("layui-nav-itemed");
	})

	//退出
	$(".signOut").click(function() {
		window.sessionStorage.removeItem("token");
	})

})

//打开新窗口
function addTab(_this) {
	tab.tabAdd(_this);
}